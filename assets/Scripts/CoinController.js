
cc.Class({
    extends: cc.Component,

    properties: {
        UI_Node:cc.Node,
        //playerNode: cc.Node,
    },

    onLoad () {
        this.playonce = false;
        this.playerNode = cc.find("Canvas/Character - si");
    },

    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.PlayerController && !this.playonce && otherCollider.tag === 2){
            //cc.log("collision");
            this.playonce = true;
            this.UI_Node.getComponent("UIManager").score+=100;
            this.playerNode.getComponent("PlayerController").playsound = true;
            this.node.destroy();
            // setTimeout(function () {
            //     this.node.destroy();
            //   }.bind(this), 100);
            //this.node.getComponent(cc.Animation).play();
        }
    }
});
