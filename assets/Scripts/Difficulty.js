// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        api: cc.Node,
        title: cc.Label,
        //jsonfile: {default: null, type: cc.JsonAsset},
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        if(Global.lang == -1){
            let tempLang = this.api.getComponent("APIConnection").getURLParameter("lang");
            if(tempLang === null){
                Global.lang = 0;
            }
            else if(tempLang === "es"){
                Global.lang = 0;
            }
            else if(tempLang === "en"){
                Global.lang = 1;
            }
            else if(tempLang === "po" || tempLang === "br"){
                Global.lang = 2;
            }
        }else{
            this.language = Global.lang;
        }
        var volcado = this.jsonfile.json;
        volcado.dialogues[this.language].info.locale;
        this.title.string = volcado.dialogues[this.language].messages[25];
    },

    EasyBtn(){
        Global.difficult = false;
        this.node.active = false;
        console.log("ez");
    },

    DifficultBtn(){
        Global.difficult = true;
        this.node.active = false;
    },

    // update (dt) {},
});
