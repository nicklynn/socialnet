// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        sceneid: {default: 0, typeof: cc.Int, visible: true},
        api: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.playerNode = cc.find("Canvas/Character - si");
        this.story = cc.find("Canvas/dialoguebox");
    },

    start () {

    },

    // update (dt) {},
    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.PlayerController && !this.playonce){
            if (this.sceneid == 3){
                console.log("sending score from easy boss: " + Global.puntaje);
                //this.api.getComponent("APIConnection").sendResult(); 
            }
            /*setTimeout(function () {
                this.PhysicsBoxCollider.enabled = false;
              }.bind(this), 100);*/
            setTimeout(function () {
                this.story.x = this.playerNode.x;
                this.story.y = 120;
                this.story.getComponent("StoryScript").state = 2;
                }.bind(this), 700);
        }
    },
});
