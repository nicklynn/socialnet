// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        jsonfile: {default: null, type: cc.JsonAsset},
        storyString: cc.Label,
        sceneId: {default: 0, typeof: cc.Int, visible: true},
        nextBtnObj: cc.Node,
        api: cc.Node,
        storyScript: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.playerNode = cc.find("Canvas/Character - si");
        this.story = cc.find("Canvas/dialoguebox2");
        this.index = -1;
    },

    start () {
        this.node.active = false;
        var volcado = this.jsonfile.json;
        this.storyString.string = volcado.dialogues[this.storyScript.getComponent("StoryScript").language].messages[this.index];
    },

    Collided(){
        this.node.active = true;
        var volcado = this.jsonfile.json;
        this.storyScript.getComponent("StoryScript").settingsNode.active = true;
        this.storyString.string = volcado.dialogues[this.storyScript.getComponent("StoryScript").language].messages[this.index];
    },

    update (dt) {
        if(this.storyScript.getComponent("StoryScript").languageChange && this.node.activeInHierarchy){
            console.log("language changed");
            var volcado = this.jsonfile.json;
            if (this.storyScript.getComponent("StoryScript").index <= 2 && this.storyScript.getComponent("StoryScript").index >= 4 || this.storyScript.getComponent("StoryScript").index <= 5 && this.storyScript.getComponent("StoryScript").index >= 7 || this.storyScript.getComponent("StoryScript").index < 8){
                this.storyString.string = volcado.dialogues[Global.lang].messages[this.index];
                this.storyScript.getComponent("StoryScript").string = volcado.dialogues[Global.lang].messages[this.index];
            } else{
                this.storyScript.getComponent("StoryScript").string = "";

            }
            this.storyString.string = volcado.dialogues[this.storyScript.getComponent("StoryScript").language].messages[this.index];
            this.storyScript.getComponent("StoryScript").languageChange = false;
            cc.director.pause();
        }
    },

    NextBtn(){
        this.story.active = false;
        this.nextBtnObj.active = false;
        this.node.active = false;
        cc.director.resume();
        this.storyScript.getComponent("StoryScript").settingsNode.active = false;
    },
});
