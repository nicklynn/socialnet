// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        camera: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    update (dt) {
        let target_position = this.camera.getPosition();
        //target_position.y = cc.misc.clampf(target_position.y, -90, 1);
        let current_position = this.node.getPosition();
        current_position.lerp(target_position , 0.9, current_position);
        this.node.setPosition(current_position);
        //this.position.x= this.camera.position.x;
    },
});
