// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        //playerNode: cc.Node,
        checkNum: {default: 0, typeof: cc.Int, visible: true},
        //deathZoneData: cc.Node,
        //rb: {default: null, type: cc.rigidBody},
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // si mi personaje tiene un número menor al checkpoint, lo agrego y mando mi posición a deathzone

    onLoad () {
        this.checkPosition = this.node.getPosition();
        this.playerNode = cc.find("Canvas/Character - si");
        this.deathZoneData = cc.find("Canvas/DeathZone");
        this.CheckSound = this.node.getComponent(cc.AudioSource);
        //this.playsound = false;
    },

    start () {

    },

    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.PlayerController && !this.playonce){
            //if (!this.playsound) {
                this.CheckSound.play();
                this.node.color = new cc.Color(16, 69, 69)
                //this.playsound = true;
            //}
            this.playerNode.getComponent("PlayerController").enableRigidbody();
            //console.log("checkpoint touched");
            this.playonce = true;
            if(this.playerNode.getComponent("PlayerController").checkpoint<=this.checkNum){
                this.deathZoneData.getComponent("DeathZone").lastCheckpointPosition = this.checkPosition;
                //console.log("checkpoint passed");
            }
            //this.UI_Node.getComponent("UIManager").score+=100;
            //this.playerNode.getComponent("PlayerController").playsound = true;
        }
    }

    // update (dt) {},
});
