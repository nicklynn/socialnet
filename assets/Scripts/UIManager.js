// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        scoreText: cc.RichText,
        score: {default: 0, typeof: cc.Int, visible: true},
        buttonLeft: cc.Node,
        buttonRight: cc.Node,
        buttonJump: cc.Node,
        buttonAttack: cc.Node,
        instructions1: cc.Node,
        instructions2: cc.Node,
        backgroundMusic: cc.Node,
        //consoleText: cc.Label,
    },

    onLoad(){
        //this.consoleText.string = "on this system " + cc.sys.os;
        //console.log("on this system " + cc.sys.os);
        if (cc.sys.os == cc.sys.MOBILE_BROWSER || cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID){
            console.log("on mobile");
            //consoleText.getComponent(cc.Label).string = "on mobile";
            this.instructions1.active = false;
            this.instructions2.active = false;
        } else if (cc.sys.os == cc.sys.DESKTOP_BROWSER || cc.sys.os == cc.sys.OS_WINDOWS || cc.sys.os == cc.sys.OS_OSX || cc.sys.os == cc.sys.LINUX){
            console.log("on browser");
            //consoleText.getComponent(cc.Label).string = "on browser";
            this.buttonLeft.active = false;
            this.buttonRight.active = false;
            this.buttonJump.active = false;
            this.buttonAttack.active = false;
            //this.backgroundMusic = cc.find("Canvas/BackgroundMusic");
            //this.muted = false;
        }
        //console.log("score global en la carga de nueva escena " + Global.puntaje);
        this.score += Global.puntaje;
    },

    /*start () {
        this.score += Global.puntaje;
    },*/

    update (dt) {
        this.scoreText.string = "Pts" + this.score;
        //Global.puntaje += this.score;
        //this.scoreText.string = "Pts" + Global.puntaje;
    },

    MuteBtn(){
        if(!Global.muted){
            //console.log("mute");
            Global.muted = true;
            cc.audioEngine.setMusicVolume(0);
            this.backgroundMusic.getComponent(cc.AudioSource).stop();
            //cc.audioEngine.pauseAll();
        }else{
            Global.muted = false;
            //cc.audioEngine.resumeAll();
            this.backgroundMusic.getComponent(cc.AudioSource).play();
            cc.audioEngine.setMusicVolume(1);
        }
    },
});
