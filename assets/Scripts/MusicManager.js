// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        musicClip: {default: [], type: [cc.AudioClip]},
        sceneId: {default: 0, typeof: cc.Int, visible: true},
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.gameSound = this.node.getComponent(cc.AudioSource);
        
    },

    start () {
        if(this.sceneId == 0){
            this.gameSound.clip(this.musicClip[0]);
        } else if (this.sceneId == 1){
            this.gameSound.clip(this.musicClip[1]);
        } else{
            this.gameSound.clip(this.musicClip[2]);
        }
        this.gameSound.play();
    },

    update (dt) {
        if(Global.muted){
            this.gameSound.stop();
        }else{
            this.gameSound.play();
        }
    },
});
