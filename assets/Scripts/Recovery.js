
cc.Class({
    extends: cc.Component,

    properties: {
        //playerNode: cc.Node,
        //vida1: cc.Node,
        //vida2: cc.Node,
        //vida3: cc.Node,
        //deathZone: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.deathZone = cc.find("Canvas/DeathZone");
        this.vida1 = cc.find("Canvas/Main Camera/vida/vida3");
        this.vida2 = cc.find("Canvas/Main Camera/vida/vida2");
        this.vida3 = cc.find("Canvas/Main Camera/vida/vida1");
        this.playerNode = cc.find("Canvas/Character - si");
    },

    start () {

    },
    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.PlayerController && otherCollider.tag === 2){
            if (this.deathZone.getComponent("DeathZone").vida == 0){
                this.deathZone.getComponent("DeathZone").vida = 1;
                this.vida3.active = true;
            }
            else if (this.deathZone.getComponent("DeathZone").vida == 1){
                this.deathZone.getComponent("DeathZone").vida = 2;
                this.vida2.active = true;
            }            
            else if (this.deathZone.getComponent("DeathZone").vida == 2){
                this.deathZone.getComponent("DeathZone").vida = 3;
                this.vida1.active = true;
            }
            this.playerNode.getComponent("PlayerController").playsound = true;
            setTimeout(function () {
                this.node.active = false;
              }.bind(this), 100);
        }
    },
    // update (dt) {},
});
