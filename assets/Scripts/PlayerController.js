

cc.Class({
    extends: cc.Component,

    properties: {
        //Character jump Height
        jumpHeight: 0,

        //Character jump duration
        jumpDuration: 0,

        //Character max movement speed
        maxMoveSpeed: 0,

        //acceleration
        accel: 0,
        checkpoint: { default: 0, typeof: cc.Int, visible: true },
        animState: cc.AnimState,
        deadClip: cc.AudioClip,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        //might look forward to make this mobile or both systems
        //key events
        this.node.PlayerController = this;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyPressed, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyReleased, this);
        this.Direction = 0;
        this.Velocity_Max_X = 400;
        this.Rigid_Body = this.node.getComponent(cc.RigidBody);
        this.Walk_Force = 15000;
        this.Jump_Force = 500000;
        this.Grounded = false;
        this.flag = 0;
        this.Coin = this.node.getComponent(cc.AudioSource);
        this.playsound = false;
        this.anim = this.getComponent(cc.Animation);
        this.attacking = false;
        this.attackingInRange = false;
        this.deathZoneData = cc.find("Canvas/DeathZone");
        this.attackSound = cc.find("Canvas/Character - si/music");
        this.moveCounter = 0;
        this.jumpAttackCounter = 0;
        this.moveCounterBool = false;
        this.jumpAttackCounterBool = false;
        this.blockCount = false;
        this.blockAttack = false;//bloquear el ataque comienza falso
        this.isJump = false;
        this.isMobile = false;
        this.tiempoAtk = 0;
        this.demoraAtk = 0.5;
        this.estaAtacando = false;
        //releaseBtn();
        //releaseAttackOrJump();
        // this.jumpAction = this.setJumpAction();
        // this.node.runAction(this.jumpAction);
    },

    onKeyPressed(event) {
        let key_code = event.keyCode;
        switch (key_code) {
            default:
                break;
            case cc.macro.KEY.left:
            case cc.macro.KEY.a:
                //if (this.flag != 1) {
                if (this.Direction != -1) {
                    //this.flag = 1;
                    this.Direction = -1;
                    if(!this.estaAtacando){
                        this.animState = this.anim.play('runleft');
                        this.animState.wrapMode = cc.WrapMode.Loop;
                        this.animState.speed = 1;
                        this.animState.repeatCount = Infinity;
                    }
                }
                break;
            case cc.macro.KEY.right:
            case cc.macro.KEY.d:
                //if (this.flag != 2) {
                if (this.Direction != 1) {
                    //this.flag = 2;
                    this.Direction = 1;
                    if(!this.estaAtacando){
                        this.animState = this.anim.play('run');
                        this.animState.wrapMode = cc.WrapMode.Loop;
                        this.animState.speed = 1;
                        this.animState.repeatCount = Infinity;
                    }
                }
                break;
            case cc.macro.KEY.space:
                if (this.Grounded) {
                    this.Rigid_Body.applyForceToCenter(cc.v2(0, this.Jump_Force), true);
                    this.Grounded = false;
                }
                break;
            case cc.macro.KEY.l:
                this.isMobile = false;
                if (!this.blockAttack && this.tiempoAtk > this.demoraAtk){//si bloquear el ataque es falso cuando voy a atacar
                    this.blockAttack = true;//ataque se hace verdadero - eso debería hacerme atacar una sola vez
                    this.jumpAttackCounter = 0;
                    this.jumpAttackCounterBool = true;
                    //this.attackSound.getComponent(cc.AudioSource).play();
                    //this.animstate = this.anim.play('attack');
                    //this.animState.wrapMode = cc.WrapMode.Normal;
                    //this.animState.speed = 0.25;
                    this.attacking = true;
                }
                break;
        }
    },
    onKeyReleased(event){
        let key_code = event.keyCode;
        switch(key_code){
            case cc.macro.KEY.left:
            case cc.macro.KEY.a:
                //if (this.flag != 0 && this.Direction != 1) {
                if (this.Direction != 1) {
                    //this.flag = 0;
                    this.Direction = 0;
                    if(!this.estaAtacando){
                        this.animState = this.anim.play('idle');
                        this.animState.wrapMode = cc.WrapMode.Loop;
                    }
                }
                break;
            case cc.macro.KEY.right:
            case cc.macro.KEY.d:
                //if (this.flag != 0 && this.Direction != 2) {
                if (this.Direction != -1) {
                    //this.flag = 0;
                    this.Direction = 0;
                    if(!this.estaAtacando){
                        this.animState = this.anim.play('idle');
                        this.animState.wrapMode = cc.WrapMode.Loop;
                    }
                }
                break;
            case cc.macro.KEY.l:
                //this.animstate = this.anim.play('attack');
                if(this.tiempoAtk > this.demoraAtk){
                    this.estaAtacando = true;
                    this.tiempoAtk = 0;
                    this.attackSound.getComponent(cc.AudioSource).play();
                    //this.animState.speed = 0.1;
                    this.animstate = this.anim.play('attack');
                    this.animState.wrapMode = cc.WrapMode.Normal;
                    setTimeout(function () {          
                        if (this.Direction == 0) {
                            this.animState = this.anim.play('idle');
                            this.animState.speed = 1;
                            this.estaAtacando = false;
                        } else if (this.Direction == -1) {
                            this.animState = this.anim.play('runleft');
                            this.animState.speed = 1;
                            this.estaAtacando = false;
                        } else {
                            this.animState = this.anim.play('run');
                            this.animState.speed = 1;
                            this.estaAtacando = false;
                        }
                        this.animState.wrapMode = cc.WrapMode.Loop;
                    }.bind(this), 200);
                }
                break;
            case cc.macro.KEY.space:
                if(this.estaAtacando) break;
                this.animState = this.anim.play('jump');
                this.animState.wrapMode = cc.WrapMode.Normal;
                if (this.Direction == 0) {
                    this.animState = this.anim.play('idle');
                    //console.log("idle");
                } else if (this.Direction == -1) {
                    this.animState = this.anim.play('runleft');
                    //console.log("left");
                } else {
                    this.animState = this.anim.play('run');
                    //console.log("right");
                }
                this.animState.wrapMode = cc.WrapMode.Loop;
                break;
            default:
                break;
        }
    },

    leftBtn(){
        this.moveCounter = 0;
        this.blockCount = false;
        this.moveCounterBool = true;
        if (this.Direction != 1) {
            //this.flag = 1;
            this.Direction = -1;
            this.animState = this.anim.play('runleft');
            this.animState.wrapMode = cc.WrapMode.Loop;
            this.animState.repeatCount = Infinity;
        }
    },
    leftBtnDown(){
        this.moveCounter = 0;
        this.blockCount = true;
        if (this.Direction != 1) {
            //this.flag = 1;
            this.Direction = -1;
            this.animState = this.anim.play('runleft');
            this.animState.wrapMode = cc.WrapMode.Loop;
            this.animState.repeatCount = Infinity;
        }
    },
    rightBtn(){
        this.moveCounter = 0;
        this.blockCount = false;
        this.moveCounterBool = true;
        if (this.Direction != 1) {
            //this.flag = 2;
            this.Direction = 1;
            this.animState = this.anim.play('run');
            this.animState.wrapMode = cc.WrapMode.Loop;
            this.animState.repeatCount = Infinity;
        }
    },
    rightBtnDown(){
        this.moveCounter = 0;
        this.blockCount = true;
        if (this.Direction != 1) {
            //this.flag = 2;
            this.Direction = 1;
            this.animState = this.anim.play('run');
            this.animState.wrapMode = cc.WrapMode.Loop;
            this.animState.repeatCount = Infinity;
        }
    },
    attackBtn(){
        this.isMobile = true;
        if(!this.blockAttack && this.tiempoAtk > this.demoraAtk){
            this.estaAtacando = true;
            this.attackSound.getComponent(cc.AudioSource).play();
            this.animstate = this.anim.play('attack');
            this.animState.wrapMode = cc.WrapMode.Normal;
            this.tiempoAtk = 0;
            setTimeout(function () {
                if (this.Direction == 0) {
                    this.animState = this.anim.play('idle');
                } else if (this.Direction == -1) {
                    this.animState = this.anim.play('runleft');
                } else {
                    this.animState = this.anim.play('run');
                }
                this.estaAtacando = false;
                this.animState.wrapMode = cc.WrapMode.Loop;
            }.bind(this), 200);
            
            this.blockAttack = true;
            this.jumpAttackCounter = 0;
            this.jumpAttackCounterBool = true;
            this.attackSound.getComponent(cc.AudioSource).play();
            this.attacking = true;
        }
    },
    jumpBtn(){
        this.isMobile = true;
        this.isJump = true;
        this.jumpAttackCounter = 0;
        this.jumpAttackCounterBool = true;
        if(this.Grounded){
            this.Rigid_Body.applyForceToCenter(cc.v2(0, this.Jump_Force), true);
            this.Grounded = false;
        }
    },
    releaseBtn(){
        if (this.Direction != 0) {
            //this.flag = 0;
            this.Direction = 0;
            this.animState = this.anim.play('idle');
            this.animState.wrapMode = cc.WrapMode.Loop;
        }
    },
    releaseAttack(){
        /*this.attackSound.getComponent(cc.AudioSource).play();
        this.animstate = this.anim.play('attack');
        this.animState.wrapMode = cc.WrapMode.Normal;
        setTimeout(function () {
            if (this.Direction == 0) {
                this.animState = this.anim.play('idle');
            } else if (this.Direction == -1) {
                this.animState = this.anim.play('runleft');
            } else {
                this.animState = this.anim.play('run');
            }
            this.animState.wrapMode = cc.WrapMode.Loop;
        }.bind(this), 200);*/
    },
    releaseJump(){
        if (this.isJump){
            this.animState = this.anim.play('jump');
            this.animState.wrapMode = cc.WrapMode.Normal;
        }
        if(this.estaAtacando) return;
        setTimeout(function () {
            if (this.Direction == 0) {
                this.animState = this.anim.play('idle');
            } else if (this.Direction == -1) {
                this.animState = this.anim.play('runleft');
            } else {
                this.animState = this.anim.play('run');
            }
            this.isJump = false;
        }.bind(this), 290);
    },
    

    onBeginContact(contact, selfCollider, otherCollider) {
        if (selfCollider.tag === 2) {
            this.Grounded = true;
            if (this.Direction == 1) {
                this.animState = this.anim.play('run');
                this.animState.wrapMode = cc.WrapMode.Loop;
            } else if (this.Direction == 0) {
                this.animState = this.anim.play('idle');
                this.animState.wrapMode = cc.WrapMode.Loop;
            } else if (this.Direction == -1) {
                this.animState = this.anim.play('runleft');
                this.animState.wrapMode = cc.WrapMode.Loop;
            }
        }
    },

    start() {
        this.animState = this.anim.play('idle');
    },

    update (dt) {
        if (!this.blockCount){
            this.moveCounter++;
        }
        
        //console.log(this.Direction);

        this.jumpAttackCounter++;
        if(this.moveCounter >= 1 && this.moveCounterBool){
            this.moveCounterBool = false;
            this.releaseBtn();
        }
        if(this.jumpAttackCounter >= 30 && this.jumpAttackCounterBool){
            this.jumpAttackCounterBool = false;
            if (this.isMobile){
                if (this.isJump){
                    this.releaseJump();
                }else{
                    this.releaseAttack();
                }
            }
        }
        if((this.Direction > 0 && this.Rigid_Body.linearVelocity.x < this.Velocity_Max_X) || (this.Direction < 0 && this.Rigid_Body.linearVelocity.x > -this.Velocity_Max_X)){
            this.Rigid_Body.applyForceToCenter(cc.v2(this.Direction*this.Walk_Force, 0), true);
        }
        if (this.playsound) {
            this.Coin.play();
            this.playsound = false;
        }
        this.tiempoAtk += dt;
        if (this.attacking) {
            //this.attackSound.getComponent(cc.AudioSource).play();
            this.attacking = false;
            this.attackingInRange = true;
            setTimeout(function () {
                this.attackingInRange = false;
            }.bind(this), 700);
            setTimeout(function () {
                this.blockAttack = false;//ataque vuelve a hacerse falso después de 800 unidades de tiempo desde que attacking es verdadero
            }.bind(this), 800);
            //matar al enemigo
        }
        if (this.deathZoneData.getComponent("DeathZone").vida == 0) {
            this.deathZoneData.getComponent("DeathZone").vida = -1;
            this.Coin.clip = this.deadClip;
            this.Coin.play();
            this.animstate = this.anim.play('die');
            this.animState.wrapMode = cc.WrapMode.Normal;
            //console.log("reloading");
            setTimeout(function () {
                this.reloadScene();
            }.bind(this), 500);
            //recargar
        }
    },

    setJumpAction() {

        cc.log(cc);
        cc.log(cc.director.getPhysicsManager());
        var jumpUp = cc.moveBy(this.jumpDuration, cc.Vec2(0, this.jumpHeight)).easing(cc.easeExponentialOut());

        var jumpDown = cc.moveBy(this.jumpDuration, cc.Vec2(0, -this.jumpHeight)).easing(cc.easeExponentialIn());

        return cc.repeatForever(cc.sequence(jumpUp, jumpDown));
    },

    enableRigidbody: function () {
        this.Rigid_Body.enabled = true;
    },

    disableRigidbody: function () {
        this.Rigid_Body.enabled = false;
    },

    reloadScene: function() {
        var sceneName;
        var _sceneInfos = cc.game._sceneInfos;
        for (var i = 0; i < _sceneInfos.length; i++) {
            if (_sceneInfos[i].uuid == cc.director._scene._id) {
                sceneName = _sceneInfos[i].url;
                sceneName = sceneName.substring(sceneName.lastIndexOf('/') + 1).match(/[^\.]+/)[0];
            }

        }
        cc.director.loadScene(sceneName);
        //return sceneName;
    }

});
