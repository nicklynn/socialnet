
cc.Class({
    extends: cc.Component,

    properties: {
        storyString: cc.Label,
        jsonfile: {default: null, type: cc.JsonAsset},
        sceneId: {default: 0, typeof: cc.Int, visible: true},
        nextBtnObj: cc.Node,
        api: cc.Node,
        block: cc.Node,
        languagePanel: cc.Node,
        settingsNode: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.resume();
    },

    start () {
        this.settingsNode.active = true;
        this.languagePanel.active = false;
        var volcado = this.jsonfile.json;
        cc.macro.ENABLE_WEBGL_ANTIALIAS=false;
        this.SpriteComponent = this.node.getComponent(cc.Sprite);
        this.language = 0;
        this.state = 0;
        this.index = -1;
        this.manager = cc.find("Canvas");
        this.storyString.string = volcado.dialogues[this.language].messages[this.index];
        this.playerNode = cc.find("Canvas/Character - si");
        setTimeout(function () {
            cc.director.pause();
          }.bind(this), 900);
        if (this.sceneId == 1){
            this.index = 0;
        } else if (this.sceneId == 2){
            this.index = 4;
        } else{
            this.finalScreen = cc.find("Canvas/FinalScoreBox");
            this.finalScoreText = cc.find("Canvas/FinalScoreBox/FinalScore");
            this.finalScoreTitle = cc.find("Canvas/FinalScoreBox/TotalScoreTitle");
            this.finalScreen.active = false;
            this.index = 7;
        }
        this.state = 0;
        //idioma
        this.languageChange = false;
        if(Global.lang == -1){
            let tempLang = this.api.getComponent("APIConnection").getURLParameter("lang");
            if(tempLang === null){
                Global.lang = 0;
            }
            else if(tempLang === "es"){
                Global.lang = 0;
            }
            else if(tempLang === "en"){
                Global.lang = 1;
            }
            else if(tempLang === "po" || tempLang === "br"){
                Global.lang = 2;
            }
        }else{
            this.language = Global.lang;
        }
        volcado.dialogues[Global.lang].info.locale;
        this.storyString.string = volcado.dialogues[Global.lang].messages[this.index];
        this.finalScoreTitle.string = volcado.dialogues[Global.lang].messages[26];
        console.log("language " + this.language + "global " + Global.lang + "temp " + tempLang);
    },

    update (dt) {
        if(this.state == 2){
            if (this.sceneId <= 2){
                var volcado = this.jsonfile.json;
                //this.language = Global.lang;
                this.storyString.string = volcado.dialogues[this.language].messages[this.index];
                this.SpriteComponent.enabled = true;
                this.nextBtnObj.active = true;
            } else{
                this.nextBtnObj.active = false;
                this.finalScreen.active = true;
                this.finalScreen.x = this.playerNode.x;
                Global.puntaje = this.manager.getComponent("UIManager").score;
                this.finalScoreText.getComponent(cc.Label).string = Global.puntaje;
                console.log("Sending score from storyscript:" + Global.puntaje);
                this.api.getComponent("APIConnection").sendResult();
            }
            cc.director.pause();
        }
    },

    settingsBtn(){
        this.languagePanel.active = true;
        this.languagePanel.x = this.playerNode.x;
        cc.director.pause();
    },
    espBtn(){
        var volcado = this.jsonfile.json;
        Global.lang = 0;
        this.language = Global.lang;
        this.languagePanel.active = false;
        volcado.dialogues[Global.lang].info.locale;
        if (this.index <= 2 && this.index >= 4 || this.index <= 5 && this.index >= 7 || this.index < 8){
            console.log("index " + this.index);
            this.storyString.string = volcado.dialogues[Global.lang].messages[this.index];
        } else{
            this.storyString.string = "";
            this.settingsNode.active = false;
        }
        this.languageChange = true;
        cc.director.resume();
    },
    engBtn(){
        var volcado = this.jsonfile.json;
        Global.lang = 1;
        this.language = Global.lang;
        volcado.dialogues[Global.lang].info.locale;
        this.languagePanel.active = false;
        if (this.index <= 2 && this.index >= 4 || this.index <= 5 && this.index >= 7 || this.index < 8){
            this.storyString.string = volcado.dialogues[Global.lang].messages[this.index];
        } else{
            this.storyString.string = "";
            this.settingsNode.active = false;
        }
        this.languageChange = true;
        cc.director.resume();
    },
    poBtn(){
        var volcado = this.jsonfile.json;
        Global.lang = 2;
        this.language = Global.lang;
        volcado.dialogues[Global.lang].info.locale;
        this.languagePanel.active = false;
        if (this.index <= 2 && this.index >= 4 || this.index <= 5 && this.index >= 7 || this.index < 8){
            this.storyString.string = volcado.dialogues[Global.lang].messages[this.index];
        } else{
            this.storyString.string = "";
            this.settingsNode.active = false;
        }
        this.languageChange = true;
        cc.director.resume();
    },

    NextBtn(){
        var volcado = this.jsonfile.json;
        this.language = Global.lang;
        console.log("language " + this.language + "global" + Global.lang);
            if(this.index == 2 || this.index == 6 || this.index == 8)
                cc.director.resume();

        if(this.index <= 1 || this.index >3 && this.index <=5 || this.index >6 && this.index <= 7){
            if(this.state != 2){
                this.index++;
                this.storyString.string = volcado.dialogues[this.language].messages[this.index];
            }
            else{
                this.state = 0;
                    this.nextBtnObj.setPosition(10000, 10000);
                    this.nextBtnObj.active = false;
                    this.storyString.string = "Loading";
                    Global.puntaje = this.manager.getComponent("UIManager").score;
                  if(this.index >= 3 && this.index <6){
                    cc.director.loadScene("level2");
                  }
                  else if(this.index >= 6 && this.index <8){
                    cc.director.loadScene("level3");
                  }
                  else {
                      this.finalScreen.active = true;
                      this.finalScoreText.string = Global.puntaje;
                      this.api.getComponent("APIConnection").sendResult();
                  }
            }
        }
        else if (this.index == 2 && this.state == 0 || this.index == 6 && this.state == 0 || this.index == 8 && this.state == 0){
            //último diálogo nivel 1 || diálogo 3 escena 2 || diálogo final nivel 3
            //this.index = 1;
            this.index++;
            this.state = 1;
            //console.log("estado 1");
            this.storyString.string = "";
            this.settingsNode.active = false;
            //console.log("string en blanco");
            this.SpriteComponent.enabled = false;
            this.nextBtnObj.active = false;
            this.block.active = false;
        } else if (this.index >= 3 && this.index <6 && this.state == 2){
        //cc.director.pause();
            //console.log("nos movemos a la escena 2");
            this.state = 0;
            Global.puntaje += this.manager.getComponent("UIManager").score;
            //console.log("score global antes de cambiar de escena" + Global.puntaje);
            this.nextBtnObj.setPosition(10000, 10000);
            this.nextBtnObj.active = false;
              this.storyString.string = "";
              this.settingsNode.active = false;
              //this.index++;
            cc.director.loadScene("level2");
        } 
        else if (this.index >= 6 && this.index < 8 && this.state == 2){
            this.state = 0;
            //Global.puntaje += this.manager.getComponent("UIManager").score;
            //console.log("score global antes de cambiar de escena" + Global.puntaje);
            this.nextBtnObj.setPosition(10000, 10000);
            this.nextBtnObj.active = false;
              this.storyString.string = "";
              this.settingsNode.active = false;
              //this.index++;
            cc.director.loadScene("level3");
        } else if (this.index >= 8 && this.state == 2){
            this.nextBtnObj.active = false;
            this.finalScreen.active = true;
            this.finalScoreText.string = Global.puntaje;
            this.api.getComponent("APIConnection").sendResult();
            //console.log("puntaje global en el if final" + Global.puntaje);
        }
    },
});
//index 0 - index 1 - index 2 - index 3 but hide (external force unhides) - change scene to index 4
