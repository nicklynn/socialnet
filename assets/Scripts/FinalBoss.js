

cc.Class({
    extends: cc.Component,

    properties: {
        lastCheckpointPosition: cc.vec2,
        animState: cc.AnimState,
        sceneid: {default: 0, typeof: cc.Int, visible: true},
        api: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.bosslife = 3;
        this.enemySound = this.node.getComponent(cc.AudioSource);
        this.playonce = false;
        this.playerNode = cc.find("Canvas/Character - si");
        this.story = cc.find("Canvas/dialoguebox");
        this.placer = cc.find("Canvas/Nivel1/Placer");
        this.vida1 = cc.find("Canvas/Main Camera/vida/vida3");
        this.vida2 = cc.find("Canvas/Main Camera/vida/vida2");
        this.vida3 = cc.find("Canvas/Main Camera/vida/vida1");
        this.deathZone = cc.find("Canvas/DeathZone");
        this.anim = this.getComponent(cc.Animation);
        this.waitForNextAttack = false;
        this.letdie = false;
    },

    start () {

    },

    // update (dt) {},

    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.PlayerController && !this.playonce){
            if (this.bosslife <= 0 && !this.letdie){
                console.log("dead boss");
                if(this.sceneid == 1){
                    this.animState = this.anim.play('enemy1die');
                } else if (this.sceneid == 2){
                    this.animState = this.anim.play('enemy2die');
                } else{
                    this.animState = this.anim.play('enemy3die');
                    console.log("Sending score from hard boss: " + Global.puntaje);
                    //this.api.getComponent("APIConnection").sendResult();
                }
                this.letdie = true;
                this.playonce = true;
                
                /*setTimeout(function () {
                    this.PhysicsBoxCollider.enabled = false;
                  }.bind(this), 100);*/
                setTimeout(function () {
                    //this.story.position = this.placer.position;
                    //this.story.position = this.playerNode.position;
                    this.story.x = this.playerNode.x;
                    this.story.y = 120;
                    //transform.position.x = josh.position.x + (mark.position.x - josh.position.x) / 2;
                    this.story.getComponent("StoryScript").state = 2;
                  }.bind(this), 700);
            } else{
                if(this.playerNode.getComponent("PlayerController").attackingInRange){
                    this.playerNode.getComponent("PlayerController").attackingInRange = false;
                    this.bosslife--;
                    if(this.sceneid == 1){
                        this.animState = this.anim.play('enemy1hurt');
                    } else if (this.sceneid == 2){
                        this.animState = this.anim.play('enemy2hurt');
                    } else{
                        this.animState = this.anim.play('enemy3hurt');
                    }
                    this.waitForNextAttack = true;
                    setTimeout(function () {
                        if(this.sceneid == 1){
                            this.animState = this.anim.play('enemy1attack');
                        } else if (this.sceneid == 2){
                            this.animState = this.anim.play('enemy2attack');
                        } else{
                            this.animState = this.anim.play('enemy3attack');
                        }
                        this.waitForNextAttack = false;
                      }.bind(this), 500);
                    console.log("loosing lifes: " + this.bosslife);
                    /*setTimeout(function () {
                        this.node.active = false;
                      }.bind(this), 100);*/
                } else{
                    if (!this.waitForNextAttack && !this.letdie){
                        this.waitForNextAttack = true;
                        if (this.deathZone.getComponent("DeathZone").vida == 3){
                            this.deathZone.getComponent("DeathZone").vida = 2;
                            this.vida1.active = false;
                        }
                        else if (this.deathZone.getComponent("DeathZone").vida == 2){
                            this.deathZone.getComponent("DeathZone").vida = 1;
                            this.vida2.active = false;
                        }            
                        else if (this.deathZone.getComponent("DeathZone").vida == 1){
                            this.deathZone.getComponent("DeathZone").vida = 0;
                            this.vida3.active = false;
                        }
                        setTimeout(function () {
                            this.waitForNextAttack = false;
                          }.bind(this), 800);
                    }
                }
            }
            if(!this.letdie){
                this.enemySound.play();
            }
            this.playonce = true;
            setTimeout(function () {
                this.playonce = false;
              }.bind(this), 100);
        }
    }
});
