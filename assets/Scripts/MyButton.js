// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        //texto: cc.Label,
        action: 0,
        normal: {default: null, type: cc.SpriteFrame},
        pressed: {default: null, type: cc.SpriteFrame},
        imagen: cc.Sprite,
    },

    start () {
        this.player = cc.find("Canvas/Character - si").getComponent("PlayerController");
        this.node.on(cc.Node.EventType.TOUCH_START, function (event) {
            this.imagen.spriteFrame = this.pressed;
            //this.texto.string = "presionado";
            switch(this.action){
                case 0:
                    this.player.leftBtnDown();
                    break;
                case 1:
                    this.player.rightBtnDown();
                    break;
                case 2:
                    this.player.attackBtn();
                    break;
                case 3:
                    this.player.jumpBtn();
                    break;
            }
            
        }, this);
        this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
            this.imagen.spriteFrame = this.normal;
            //this.texto.string = "finalizo";
            switch(this.action){
                case 0:
                case 1:
                    this.player.releaseBtn()();
                    break;
                case 2:
                case 3:
                    this.player.releaseAttackOrJump();
                    break;
            }
            
        }, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {
            this.imagen.spriteFrame = this.normal;
            //this.texto.string = "finalizo";
            switch(this.action){
                case 0:
                case 1:
                    this.player.releaseBtn()();
                    break;
                case 2:
                case 3:
                    this.player.releaseAttackOrJump();
                    break;
            }
            
        }, this);
    },
});
