// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        //playerNode: cc.Node,
        lastCheckpointPosition: cc.vec2,
        //vida1: cc.Node,
        //vida2: cc.Node,
        //vida3: cc.Node,
        //deadClip: cc.AudioClip,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.enemySound = this.node.getComponent(cc.AudioSource);
        this.playonce = false;
        this.moveToCheckPoint = false;
        this.playerNode = cc.find("Canvas/Character - si");
        this.vida1 = cc.find("Canvas/Main Camera/vida/vida3");
        this.vida2 = cc.find("Canvas/Main Camera/vida/vida2");
        this.vida3 = cc.find("Canvas/Main Camera/vida/vida1");
        this.deathZone = cc.find("Canvas/DeathZone");
        this.attackTime = 700;
    },

    start () {
        if (Global.difficult){
            this.attackTime = 700;
        } else{
            this.attackTime = 900;
        }
    },

    update (dt) {
        if(this.moveToCheckPoint){
            this.playerNode.position = this.lastCheckpointPosition;
            this.moveToCheckPoint = false;
        }
    },

    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.PlayerController && !this.playonce){
            if(this.playerNode.getComponent("PlayerController").attackingInRange){
                this.playonce = true;
                this.playerNode.getComponent("PlayerController").attackingInRange = false;
                setTimeout(function () {
                    this.node.active = false;
                  }.bind(this), 100);
            } else{
                if (this.deathZone.getComponent("DeathZone").vida == 3){
                    this.deathZone.getComponent("DeathZone").vida = 2;
                    this.vida1.active = false;
                }
                else if (this.deathZone.getComponent("DeathZone").vida == 2){
                    this.deathZone.getComponent("DeathZone").vida = 1;
                    this.vida2.active = false;
                }            
                else if (this.deathZone.getComponent("DeathZone").vida == 1){
                    this.deathZone.getComponent("DeathZone").vida = 0;
                    this.vida3.active = false;
                    this.moveToCheckPoint = true;
                }
                this.playonce = true;
                setTimeout(function () {
                    this.playonce = false;
                  }.bind(this), this.attackTime);
            }
            this.enemySound.play();
        }
    },
});
