// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        id: 0,
        collider: cc.BoxCollider,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //poner este script en cada botón
        this.node.on(cc.Node.EventType.TOUCH_START, this.TouchStart, this);
        //this.node.on(cc.Node.EventType.TOUCH_END, this.TouchEnd, this);
        this.playerNode = cc.find("Canvas/Character - si");
        //this.collider = cc.Node.BoxCollider;
        cc.macro.ENABLE_MULTI_TOUCH = true;
    },

    start () {
        cc.director.getCollisionManager().enabled = true;
    },

    // update (dt) {},

    TouchStart(event){
        if (this.id == 0){
            //si estoy en el botón, mantener la acción de moverse hasta el up
            this.playerNode.getComponent("PlayerController").leftBtnDown();
        } else if (this.id == 1){
            this.playerNode.getComponent("PlayerController").rightBtnDown();
        }
    },
    //TouchEnd(event){
        //stop character
    //    if (cc.Intersection.pointInPolygon(this.local_touch_pos, this.collider.world.points) {
            // node has been touched; add code here
    //        if (this.id == 0 || this.id == 1){
    //            this.playerNode.getComponent("PlayerController").releaseBtn();
    //        } 
    //    }
    //},
});
