// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        //playerNode: cc.Node,
        lastCheckpointPosition: cc.vec2,
        vida1: cc.Node,
        vida2: cc.Node,
        vida3: cc.Node,
        vida: {default: 3, typeof: cc.Int, visible: true},
        //rb: {default: null, type: cc.rigidBody},
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.deathSound = this.node.getComponent(cc.AudioSource);
        this.playonce = false;
        this.moveToCheckPoint = false;
        this.playerNode = cc.find("Canvas/Character - si");
        //this.node.getComponent(cc.RigidBody);
    },

    start () {

    },

    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.PlayerController /*&& !this.playonce*/){
            console.log("player");
            //this.playonce = true;
            if (this.vida === 3){
                this.vida = 2;
                this.vida1.active = false;
            }
            else if (this.vida === 2){
                this.vida = 1;
                this.vida2.active = false;
            }            
            else if (this.vida === 1){
                this.vida = 0;
                this.vida3.active = false;
            }
            console.log("last position " + this.lastCheckpointPosition);
            //this.playerNode.setPosition(lastCheckpointPosition);
            //this.playerNode.getComponent("PlayerController").disableRigidbody();
            this.moveToCheckPoint = true;
            this.deathSound.play();
        }
    },

    update (dt) {
        if(this.moveToCheckPoint){
            //this.playerNode.getComponent("PlayerController").disableRigidbody();
            this.playerNode.position = this.lastCheckpointPosition;
            this.moveToCheckPoint = false;
        }
    },
});
