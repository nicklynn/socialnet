// Copyright 2020 (C). Omar Lleonardo Pino Reyes. All Rights Reserved
const REQUEST_TYPES = {
    APP_TOKEN: 0,
    MATCH_ID: 1,
    MATCH_UPDATE: 2
}

cc.Class({
    extends: cc.Component,

    properties: {
        // Asset that will contain all the keys for the API connection
        apiData: {
            default: null,
            type: cc.JsonAsset
        },
        isDebug: cc.Boolean,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.apiJsonData = this.apiData.json;
    },

    start() {
        let newData = this.apiJsonData.appTokenData;
        if ((Global.appToken == "" || Global.matchID == "" || Global.gameToken == "") && Global.auth == false) {
            this.restRequest(this.apiJsonData.authURL, newData, REQUEST_TYPES.APP_TOKEN);
            Global.auth = true;
        }
    },

    // update (dt) {},

    restRequest(url, data, type) {
        let http = new XMLHttpRequest();

        let stringData = JSON.stringify(data);
        //Opening the request
        http.open("POST", url, true);
        //Setting the header for the request. 
        if (!this.isDebug) {
            http.setRequestHeader("Content-Type", "application/json");
        }
        var self = this;
        //Setting the callback to receive the response from the server
        http.onreadystatechange = function () {

            var httpStatus = http.statusText;
            //Check the status of the response and print if done with the request
            if (http.readyState == 4 && (http.status >= 200 && http.status < 500)) {
                if (http.responseText) {
                    //Depending on the type save the value and do respective action
                    switch (type) {
                        case REQUEST_TYPES.APP_TOKEN:
                            try {
                                let jsonObj = JSON.parse(http.responseText);
                                if (Global.appToken == "" || Global.appToken == undefined || Global.appToken == null) {
                                    Global.appToken = jsonObj.data.appToken;
                                }
                                cc.log(jsonObj);
                                Global.gameToken = self.getURLParameter("gameToken");
                                cc.log(Global.gameToken);
                                let matchIdRequest = {
                                    "appToken": Global.appToken,
                                    "gameToken": Global.gameToken
                                };
                                cc.log(matchIdRequest);
                                self.restRequest(self.apiJsonData.matchURL, matchIdRequest, REQUEST_TYPES.MATCH_ID);
                            } catch (error) {
                                cc.log(error);
                            }
                            break;
                        case REQUEST_TYPES.MATCH_ID:
                            try {
                                let jsonObj = JSON.parse(http.responseText);
                                if (Global.matchID == "" || Global.matchID == undefined || Global.matchID == null) {
                                    Global.matchID = jsonObj.data.gameMatchID;
                                }
                                cc.log(jsonObj);
                            } catch (error) {
                                cc.log(error)
                            }
                            break;
                        case REQUEST_TYPES.MATCH_UPDATE:
                            //Do whatever is needed after the game has sbeen completed
                            let jsonObj = JSON.parse(http.responseText);
                            cc.log(jsonObj);
                            break;
                        default:
                            console.log("Nothing");
                            break;
                    }

                }
            }
        }
        //Sending the request
        http.send(stringData);
    },

    sendResult(){
        try {
            let updateJson = {
                "appToken": Global.appToken,
                "gameMatchID": Global.matchID,
                "gameMatchData": {
                    "match_result": "WIN",
                    "match_general_score": Global.puntaje
                }
            };
            this.restRequest(this.apiJsonData.updateURL, updateJson, REQUEST_TYPES.MATCH_UPDATE);
        } catch (error) {
            cc.log(error)
        }
    },

    getURLParameter(param) {
        try {
            return decodeURIComponent((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        }
        catch (e) {
            console.log('Value not found');
            return null;
        }
    },
});
