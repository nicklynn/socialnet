// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        UI_Node:cc.Node,
        nextBtnObj: cc.Node,
        tipid: {default: 0, typeof: cc.Int, visible: true},
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //this.playonce = false;
        this.playerNode = cc.find("Canvas/Character - si");
        this.story = cc.find("Canvas/dialoguebox2");

    },

    //start () {},

    // update (dt) {},
    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.PlayerController /*&& !this.playonce*/ && otherCollider.tag === 2){
            this.story.active = true;
            this.nextBtnObj.active = true;
            //this.playonce = true;
            this.UI_Node.getComponent("UIManager").score+=200;
            this.playerNode.getComponent("PlayerController").playsound = true;
            this.story.getComponent("TipsBox").index = this.tipid;
            this.story.getComponent("TipsBox").Collided();
            setTimeout(function () {
                //this.playerNode.getComponent("PlayerController").playsound = true;
                //this.story.position = this.placer.position;
                //this.story.position = this.playerNode.position;
                this.story.x = this.playerNode.x;
                this.story.y = 100;
                //transform.position.x = josh.position.x + (mark.position.x - josh.position.x) / 2;
                //this.story.getComponent("StoryScript").state = 2;
                cc.director.pause();
              }.bind(this), 100);
              //cc.director.pause();
              setTimeout(function () {
                this.node.destroy();
              }.bind(this), 1000);
        }
    },

});
